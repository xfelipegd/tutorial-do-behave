class Deparment(object):
    def __init__(self, name, members=None):
        if not members:
            members = []
        self.name = name
        self.members = members

    def _member(self, name):
        assert name not in self.members
        self.members.append(name)

    @property
    def count(self):
        return len(self.members)

    def __len__(self):
        return self.count

class CompanyModel(object):
    def __init__(self):
        self.users = []
        self.deparments = {}

    def add_user(self, name, deparment):
        assert name not in self.users
        if deparment not in self.deparments:
            self.deparments[deparment] = Deparment(deparment)
        self.deparments[deparment]._member(name)

    def count_persons_per_deparment(self):
        pass

    def get_headcount_for(self, deparment):
        return self.deparments[deparment].count
