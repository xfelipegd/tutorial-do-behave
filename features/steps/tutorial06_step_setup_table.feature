Feature: Step Setup Table (tutorial06)

    Scenario: Setup Table
        Given a set of specific users:
            | name      | deparment     |
            | Barry     | Beer Cans     |
            | Pudey     | Silly Walks   |
            | Two-Lumps | Silly Walks   |
        When we count the number of people in each deparment
        Then we will find two peoples in "Silly Walks"
        But we will find one person in "Beer Cans"
